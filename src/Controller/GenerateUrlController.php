<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Url;
use App\Service\GeneratorShortUrl\GeneratorShortenerUrl;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GenerateUrlController extends AbstractController
{
    public function __construct(
        private GeneratorShortenerUrl $generatorShortenerUrl,
        private EntityManagerInterface $entityManager,
        private ParameterBagInterface $parameterBag,
        private ValidatorInterface $validator,
        private SerializerInterface $serializer
    ) {
    }

    #[Route('/generate/url', name: 'generate_url', methods: ['POST'])]
    public function __invoke(Request $request): Response
    {
        $originalUrl = $request->request->get('url', '');

        $url = new Url();
        $url->setOriginUrl($originalUrl);

        $violations = $this->validator->validate($url);

        if (\count($violations)) {
            $json = $this->serializer->serialize($violations, 'json');

            return new JsonResponse($json, Response::HTTP_UNPROCESSABLE_ENTITY, [], true);
        }

        $uuid = $this->generatorShortenerUrl->generate($originalUrl);

        $url->setUuid($uuid);

        $this->entityManager->persist($url);
        $this->entityManager->flush();

        return $this->json([
            'url' => $this->parameterBag->get('frontend_shortened_url').$uuid,
        ]);
    }
}
