<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegistrationController extends AbstractController
{
    public function __construct(
        private ValidatorInterface $validator,
        private EntityManagerInterface $entityManager,
        private SerializerInterface $serializer,
        private UserPasswordHasherInterface $passwordHasher
    ) {
    }

    #[Route('/registration', name: 'registration', methods: ['POST'])]
    public function __invoke(Request $request): JsonResponse
    {
        $requestData = $request->request;
        $user = new User();
        $user->setEmail($requestData->get('email', ''));
        $user->setPlainPassword($requestData->get('password', ''));

        $violations = $this->validator->validate($user);

        if (\count($violations)) {
            $json = $this->serializer->serialize($violations, 'json');

            return new JsonResponse($json, Response::HTTP_UNPROCESSABLE_ENTITY, [], true);
        }

        $user->setPassword($this->passwordHasher->hashPassword($user, $user->getPlainPassword()));

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->json(['email' => $user->getEmail()]);
    }
}
