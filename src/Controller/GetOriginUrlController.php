<?php

declare(strict_types=1);

namespace App\Controller;

use App\Message\SetVisitInfoMessage;
use App\Repository\UrlRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class GetOriginUrlController extends AbstractController
{
    public function __construct(private UrlRepository $urlRepository, private MessageBusInterface $messageBus)
    {
    }

    #[Route('/get-url/{uuid}', name: 'get_origin_url', methods: ['GET'])]
    public function __invoke(Request $request): Response
    {
        $url = $this->urlRepository->findOneBy(['uuid' => $request->attributes->get('uuid')]);

        if (!$url) {
            throw new NotFoundHttpException();
        }

        $this->messageBus->dispatch(
            new SetVisitInfoMessage(
                $url->getId(),
                $request->getClientIp(),
                $request->headers->get('User-Agent', '')
            )
        );

        return new JsonResponse([
            'url' => $url->getOriginUrl(),
        ]);
    }
}
