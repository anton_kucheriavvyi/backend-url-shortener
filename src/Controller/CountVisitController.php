<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\UrlRepository;
use App\Repository\VisitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class CountVisitController extends AbstractController
{
    public function __construct(private UrlRepository $urlRepository, private VisitRepository $visitRepository)
    {
    }

    #[Route('/count/visit/{uuid}', name: 'count_visit')]
    public function __invoke(string $uuid): Response
    {
        $url = $this->urlRepository->findOneBy(['uuid' => $uuid]);

        if (!$url) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse([
            'count' => $this->visitRepository->getCountForUrl($url),
        ]);
    }
}
