<?php

declare(strict_types=1);

namespace App\Service\BrowserDetector;

interface BrowserDetectorInterface
{
    public function detect(string $userAgent): ?string;
}
