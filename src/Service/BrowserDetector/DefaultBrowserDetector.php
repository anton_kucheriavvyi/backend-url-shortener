<?php

declare(strict_types=1);

namespace App\Service\BrowserDetector;

use DeviceDetector\DeviceDetector;

class DefaultBrowserDetector implements BrowserDetectorInterface
{
    public function __construct(private DeviceDetector $deviceDetector)
    {
    }

    public function detect(string $userAgent): ?string
    {
        $this->deviceDetector->setUserAgent($userAgent);
        $this->deviceDetector->parse();

        $client = $this->deviceDetector->getClient();

        if (is_array($client)) {
            return $client['name'] ?? null;
        }

        return $client;
    }
}
