<?php

declare(strict_types=1);

namespace App\Service\GeneratorShortUrl\Algorithm;

class DefaultAlgorithm implements AlgorithmInterface
{
    public function encode(string $url): string
    {
        return hash('crc32', $url.time());
    }
}
