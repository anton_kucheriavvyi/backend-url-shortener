<?php

declare(strict_types=1);

namespace App\Service\GeneratorShortUrl\Algorithm;

interface AlgorithmInterface
{
    public function encode(string $url): string;
}
