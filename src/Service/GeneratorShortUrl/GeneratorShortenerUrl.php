<?php

declare(strict_types=1);

namespace App\Service\GeneratorShortUrl;

use App\Service\GeneratorShortUrl\Algorithm\AlgorithmInterface;

class GeneratorShortenerUrl
{
    public function __construct(private AlgorithmInterface $algorithm)
    {
    }

    public function generate(string $url): string
    {
        return $this->algorithm->encode($url);
    }
}
