<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsValidUrlToShorten extends Constraint
{
    public $message = 'Unable to shorten that link. It is not a valid url.';
}
