<?php

namespace App\Validator;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsValidUrlToShortenValidator extends ConstraintValidator
{
    public function __construct(private ParameterBagInterface $parameterBag)
    {
    }

    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint \App\Validator\IsValidUrlToShorten */

        if (null === $value || '' === $value) {
            return;
        }

        if (0 === preg_match("/(https?:\/\/)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?()&\/=]*)/", $value, $matches)
            || str_contains($value, $this->parameterBag->get('frontend_shortened_url'))
        ) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
