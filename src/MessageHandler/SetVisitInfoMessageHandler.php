<?php

namespace App\MessageHandler;

use App\Entity\Visit;
use App\Message\SetVisitInfoMessage;
use App\Repository\UrlRepository;
use App\Service\BrowserDetector\BrowserDetectorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class SetVisitInfoMessageHandler implements MessageHandlerInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private UrlRepository $urlRepository,
        private ValidatorInterface $validator,
        private BrowserDetectorInterface $browserDetector
    ) {
    }

    public function __invoke(SetVisitInfoMessage $message)
    {
        $url = $this->urlRepository->find($message->getUrlId());

        if (!$url) {
            throw new BadRequestException('Url not found');
        }

        $visit = new Visit();
        $visit->setDate(new \DateTimeImmutable());
        $visit->setIp($message->getUserIp());
        $visit->setUrl($url);
        $visit->setBrowser($this->browserDetector->detect($message->getUserAgent()));

        $violations = $this->validator->validate($visit);

        if ($violations->count()) {
            throw new ValidationFailedException($visit, $violations);
        }

        $this->entityManager->persist($visit);
        $this->entityManager->flush();
    }
}
