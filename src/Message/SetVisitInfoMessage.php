<?php

namespace App\Message;

final class SetVisitInfoMessage
{
    public function __construct(private int $urlId, private ?string $userIp, private string $userAgent)
    {
    }

    public function getUrlId(): int
    {
        return $this->urlId;
    }

    public function getUserIp(): ?string
    {
        return $this->userIp;
    }

    public function getUserAgent(): string
    {
        return $this->userAgent;
    }
}
