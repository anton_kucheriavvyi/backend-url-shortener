<?php

namespace Unit\Validator;

use App\Validator\IsValidUrlToShorten;
use App\Validator\IsValidUrlToShortenValidator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;

class IsValidUrlToShortenTest extends TestCase
{
    private MockObject $context;

    private IsValidUrlToShortenValidator $validator;

    protected function setUp(): void
    {
        $this->context = $this->getMockBuilder(ExecutionContextInterface::class)->getMock();
        $parameterBagMock = $this->createMock(ParameterBagInterface::class);
        $parameterBagMock
            ->method('get')
            ->with('frontend_shortened_url')
            ->willReturn('localhost.com');
        $this->validator = new IsValidUrlToShortenValidator($parameterBagMock);
        $this->validator->initialize($this->context);
    }

    /** @dataProvider notValidUrlsProvider */
    public function testValidateInvalidUrl($url): void
    {
        $violationBuilder = $this->getMockBuilder(ConstraintViolationBuilderInterface::class)->getMock();
        $violationBuilder->method('setParameter')->willReturnSelf();

        $this->context->expects($this->once())->method('buildViolation')->willReturn($violationBuilder);

        $this->validator->validate($url, new IsValidUrlToShorten());
    }

    /** @dataProvider validUrlsProvider */
    public function testValidateValidUrl($url): void
    {
        $this->context->expects($this->never())->method('buildViolation');

        $this->validator->validate($url, new IsValidUrlToShorten());
    }

    public function notValidUrlsProvider()
    {
        yield ['vawrawvrawr.r', false];
        yield ['foobar', false];
        yield ['http://../', false];
        yield ['http://224.1.1.1', false];
        yield ['http://142.42.1.1:8080/', false];
        yield ['localhost.com/vwra', false];
    }

    public function validUrlsProvider()
    {
        yield ['http://foo.com/blah_blah', true];
        yield ['http://foo.com/blah_blah/', true];
        yield ['http://foo.com/blah_blah_(wikipedia)', true];
        yield ['http://www.example.com/post/?p=364', true];
        yield ['https://www.example.com/foo/?bar=baz&inga=42&quux', true];
        yield ['http://userid:password@example.com:8080', true];
        yield ['http://foo.com/blah_(wikipedia)#cite-1', true];
        yield ['google.com', true];
        yield ['www.google.com', true];
        yield ['http://1337.net', true];
        yield ['http://foo.bar/?q=Test%20URL-encoded%20stuff', true];
    }
}
