<?php

declare(strict_types=1);

namespace Unit\MessageHandlers;

use App\Entity\Url;
use App\Message\SetVisitInfoMessage;
use App\MessageHandler\SetVisitInfoMessageHandler;
use App\Repository\UrlRepository;
use App\Service\BrowserDetector\BrowserDetectorInterface;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SetVisitInfoMessageHandlerTest extends TestCase
{
    public function testHandleMethod()
    {
        $userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0';
        $urlMock = $this->createMock(Url::class);

        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $entityManagerMock->expects($this->once())->method('persist');
        $entityManagerMock->expects($this->once())->method('flush');

        $urlRepositoryMock = $this->createMock(UrlRepository::class);
        $urlRepositoryMock->method('find')->with(1)->willReturn($urlMock);

        $violationsMock = $this->createMock(ConstraintViolationList::class);
        $violationsMock->method('count')->willReturn(0);

        $validatorMock = $this->createMock(ValidatorInterface::class);
        $validatorMock->method('validate')->willReturn($violationsMock);

        $defaultBrowserDetectorMock = $this->createMock(BrowserDetectorInterface::class);
        $defaultBrowserDetectorMock->method('detect')->with($userAgent)->willReturn('Firefox');

        $setVisitInfoMessage = new SetVisitInfoMessage(1, '127.0.0.1', $userAgent);

        $setVisitInfoMessageHandler = new SetVisitInfoMessageHandler($entityManagerMock, $urlRepositoryMock, $validatorMock, $defaultBrowserDetectorMock);

        $setVisitInfoMessageHandler($setVisitInfoMessage);
    }

    public function testHandleMethodWithValidationException()
    {
        $userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0';
        $urlMock = $this->createMock(Url::class);

        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $entityManagerMock->expects($this->never())->method('persist');
        $entityManagerMock->expects($this->never())->method('flush');

        $urlRepositoryMock = $this->createMock(UrlRepository::class);
        $urlRepositoryMock->method('find')->with(1)->willReturn($urlMock);

        $violationsMock = $this->createMock(ConstraintViolationList::class);
        $violationsMock->method('count')->willReturn(1);

        $validatorMock = $this->createMock(ValidatorInterface::class);
        $validatorMock->method('validate')->willReturn($violationsMock);

        $defaultBrowserDetectorMock = $this->createMock(BrowserDetectorInterface::class);
        $defaultBrowserDetectorMock->method('detect')->with($userAgent)->willReturn('Firefox');

        $setVisitInfoMessage = new SetVisitInfoMessage(1, '1231345.345345345.4566456456.12214124.5345345345', $userAgent);

        $setVisitInfoMessageHandler = new SetVisitInfoMessageHandler($entityManagerMock, $urlRepositoryMock, $validatorMock, $defaultBrowserDetectorMock);

        $this->expectException(ValidationFailedException::class);

        $setVisitInfoMessageHandler($setVisitInfoMessage);
    }
}
