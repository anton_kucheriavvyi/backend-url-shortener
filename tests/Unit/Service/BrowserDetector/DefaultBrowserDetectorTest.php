<?php

declare(strict_types=1);

namespace Unit\Service\BrowserDetector;

use App\Service\BrowserDetector\DefaultBrowserDetector;
use DeviceDetector\DeviceDetector;
use PHPUnit\Framework\TestCase;

class DefaultBrowserDetectorTest extends TestCase
{
    /** @dataProvider clientValueProvider */
    public function testDetectMethod($clientValue, $expectedValue)
    {
        $userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36';
        $deviceDetectorMock = $this->createMock(DeviceDetector::class);
        $deviceDetectorMock->expects($this->once())->method('setUserAgent')->with($userAgent);
        $deviceDetectorMock->expects($this->once())->method('parse');
        $deviceDetectorMock->expects($this->once())->method('getClient')->willReturn($clientValue);

        $defaultBrowserDetector = new DefaultBrowserDetector($deviceDetectorMock);
        $browser = $defaultBrowserDetector->detect($userAgent);

        $this->assertEquals($expectedValue, $browser);
    }

    public function clientValueProvider()
    {
        yield [['name' => 'Chrome'], 'Chrome'];
        yield ['Chrome', 'Chrome'];
    }
}
