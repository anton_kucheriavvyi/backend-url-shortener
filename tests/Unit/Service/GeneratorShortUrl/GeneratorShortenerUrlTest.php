<?php

declare(strict_types=1);

namespace Unit\Service\GeneratorShortUrl;

use App\Service\GeneratorShortUrl\Algorithm\AlgorithmInterface;
use App\Service\GeneratorShortUrl\GeneratorShortenerUrl;
use PHPUnit\Framework\TestCase;

class GeneratorShortenerUrlTest extends TestCase
{
    public function testGenerateMethod()
    {
        $url = 'https://google.com';
        $algorithmResult = 'somehash';
        $algorithmMock = $this->createMock(AlgorithmInterface::class);
        $algorithmMock->method('encode')->with($url)->willReturn($algorithmResult);

        $generator = new GeneratorShortenerUrl($algorithmMock);

        $result = $generator->generate($url);

        $this->assertEquals($algorithmResult, $result);
    }
}
