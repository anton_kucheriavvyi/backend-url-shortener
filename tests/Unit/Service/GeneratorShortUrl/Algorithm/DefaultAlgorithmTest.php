<?php

declare(strict_types=1);

namespace Unit\Service\GeneratorShortUrl\Algorithm;

use App\Service\GeneratorShortUrl\Algorithm\DefaultAlgorithm;
use PHPUnit\Framework\TestCase;

class DefaultAlgorithmTest extends TestCase
{
    public function testEncode()
    {
        $url = 'https://google.com';

        $algorithm = new DefaultAlgorithm();

        $result = $algorithm->encode($url);

        $this->assertEquals(8, strlen($result));
    }
}
