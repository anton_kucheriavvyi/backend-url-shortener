<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use App\Entity\Url;
use Doctrine\ORM\EntityManagerInterface;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CountVisitControllerTest extends WebTestCase
{
    use RefreshDatabaseTrait;

    public function testGetVisitsCount(): void
    {
        /** @var EntityManagerInterface $em */
        $client = self::createClient();
        $client->disableReboot();

        $em = self::$container->get('doctrine.orm.entity_manager');

        $router = self::$container->get('router');

        $client->jsonRequest(
            'GET',
            $router->generate('count_visit', ['uuid' => 'someuuid'])
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);

        $requestUrl = 'https://google.com';
        $client->jsonRequest('POST', $router->generate('generate_url'), [
            'url' => $requestUrl,
        ]);

        $this->assertResponseIsSuccessful();

        /** @var Url $url */
        $url = $em->getRepository(Url::class)->findOneBy(['originUrl' => $requestUrl]);

        $client->jsonRequest(
            'GET',
            $router->generate('count_visit', ['uuid' => $url->getUuid()])
        );
        $response = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals(0, $response['count']);

        $client->jsonRequest(
            'GET',
            $router->generate('get_origin_url', ['uuid' => $url->getUuid()])
        );

        $this->assertResponseIsSuccessful();

        $client->jsonRequest(
            'GET',
            $router->generate('get_origin_url', ['uuid' => $url->getUuid()])
        );

        $this->assertResponseIsSuccessful();

        $client->jsonRequest(
            'GET',
            $router->generate('get_origin_url', ['uuid' => $url->getUuid()])
        );

        $this->assertResponseIsSuccessful();

        $client->request(
            'GET',
            $router->generate('count_visit', ['uuid' => $url->getUuid()])
        );
        $response = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals(3, $response['count']);
    }
}
