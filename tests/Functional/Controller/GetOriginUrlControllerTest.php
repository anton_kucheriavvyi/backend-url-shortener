<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use App\Entity\Url;
use App\Entity\Visit;
use Doctrine\ORM\EntityManagerInterface;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GetOriginUrlControllerTest extends WebTestCase
{
    use RefreshDatabaseTrait;

    private $router;

    public function testGetOriginUrl(): void
    {
        $client = self::createClient();
        $client->disableReboot();
        $this->router = self::$container->get('router');

        $uuid = 'qwertyu';
        $originUrl = 'https://google.com';

        $url = new Url();
        $url->setUuid($uuid);
        $url->setOriginUrl($originUrl);

        /** @var EntityManagerInterface $em */
        $em = self::$container->get('doctrine.orm.entity_manager');
        $em->persist($url);
        $em->flush();

        $client->setServerParameter('HTTP_USER_AGENT', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36');
        $client->setServerParameter('REMOTE_ADDR', '197.0.0.11');

        $client->jsonRequest(
            'GET',
            $this->router->generate('get_origin_url', ['uuid' => $uuid])
        );
        $response = json_decode($client->getResponse()->getContent(), true);
        $visitInfo = $em->find(Visit::class, ['id' => 1]);

        $this->assertResponseIsSuccessful();
        $this->assertEquals($originUrl, $response['url']);
        $this->assertNotEmpty($visitInfo);
        $this->assertEquals($url->getId(), $visitInfo->getUrl()->getId());
        $this->assertEquals('Chrome', $visitInfo->getBrowser());
        $this->assertEquals('197.0.0.11', $visitInfo->getIp());
    }
}
