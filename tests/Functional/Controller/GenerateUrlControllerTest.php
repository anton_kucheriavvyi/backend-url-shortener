<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use App\Entity\Url;
use Doctrine\ORM\EntityManagerInterface;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class GenerateUrlControllerTest extends WebTestCase
{
    use RefreshDatabaseTrait;

    private $router;

    public function testGenerateUrl(): void
    {
        $client = self::createClient();
        $client->disableReboot();
        $this->router = self::$container->get('router');

        $client->jsonRequest('POST', $this->router->generate('generate_url'), [
            'url' => '',
        ]);

        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());

        $requestUrl = 'https://google.com';
        $client->jsonRequest('POST', $this->router->generate('generate_url'), [
            'url' => $requestUrl,
        ]);
        $response = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertNotEmpty($response['url']);

        /** @var EntityManagerInterface $em */
        $em = self::$container->get('doctrine.orm.entity_manager');

        /** @var Url $url */
        $url = $em->getRepository(Url::class)->findOneBy(['originUrl' => $requestUrl]);

        $parameterBag = self::$container->get('parameter_bag');

        $this->assertNotEmpty($url);
        $this->assertEquals($requestUrl, $url->getOriginUrl());
        $this->assertEquals($response['url'], $parameterBag->get('frontend_shortened_url').$url->getUuid());
    }

    /** @dataProvider urlsProvider */
    public function testUrlValidator($url, $isValid)
    {
        $client = self::createClient();
        $client->disableReboot();
        $this->router = self::$container->get('router');

        if ('localhost' === $url) {
            $parameterBag = self::$container->get('parameter_bag');
            $url = $parameterBag->get('frontend_shortened_url').'rv92';
        }

        $client->jsonRequest('POST', $this->router->generate('generate_url'), [
            'url' => $url,
        ]);

        $this->assertResponseStatusCodeSame($isValid ? Response::HTTP_OK : Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function urlsProvider()
    {
        yield ['http://foo.com/blah_blah', true];
        yield ['vawrawvrawr.r', false];
        yield ['foobar', false];
        yield ['http://foo.com/blah_blah/', true];
        yield ['http://foo.com/blah_blah_(wikipedia)', true];
        yield ['http://www.example.com/post/?p=364', true];
        yield ['https://www.example.com/foo/?bar=baz&inga=42&quux', true];
        yield ['http://userid:password@example.com:8080', true];
        yield ['http://foo.com/blah_(wikipedia)#cite-1', true];
        yield ['google.com', true];
        yield ['www.google.com', true];
        yield ['http://../', false];
        yield ['http://224.1.1.1', false];
        yield ['http://142.42.1.1:8080/', false];
        yield ['http://1337.net', true];
        yield ['http://foo.bar/?q=Test%20URL-encoded%20stuff', true];
        yield ['localhost', false];
    }
}
