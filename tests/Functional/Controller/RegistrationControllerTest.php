<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class RegistrationControllerTest extends WebTestCase
{
    use RefreshDatabaseTrait;

    public function testRegistration(): void
    {
        $client = self::createClient();
        $client->disableReboot();

        $router = self::$container->get('router');

        $client->jsonRequest('POST', $router->generate('registration'), []);

        $this->assertResponseStatusCodeSame(Response::HTTP_UNPROCESSABLE_ENTITY);

        $request = [
            'email' => 'incorrectgmail.com',
            'password' => '123456',
        ];
        $client->jsonRequest('POST', $router->generate('registration'),
            $request
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_UNPROCESSABLE_ENTITY);

        $request = [
            'email' => 'user@gmail.com',
            'password' => '',
        ];
        $client->jsonRequest('POST', $router->generate('registration'),
            $request
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_UNPROCESSABLE_ENTITY);

        $request = [
            'email' => 'test@gmail.com',
            'password' => '123456',
        ];
        $client->jsonRequest('POST', $router->generate('registration'), $request);

        $this->assertResponseIsSuccessful();

        $client->jsonRequest('POST', $router->generate('registration'), $request);

        $this->assertResponseStatusCodeSame(Response::HTTP_UNPROCESSABLE_ENTITY);

        $client->jsonRequest('POST', $router->generate('authentication_token'), $request);
    }

    public function testLogin()
    {
        $client = self::createClient();
        $client->disableReboot();

        $router = self::$container->get('router');

        $request = [
            'email' => 'test@gmail.com',
            'password' => '123456',
        ];
        $client->jsonRequest('POST', $router->generate('registration'), $request);

        $this->assertResponseIsSuccessful();

        $incorrectEmailRequest = [
            'email' => 'user@gmail.com',
            'password' => '123456',
        ];
        $client->jsonRequest('POST', $router->generate('authentication_token'), $incorrectEmailRequest);

        $this->assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);

        $incorrectPasswordRequest = [
            'email' => 'test@gmail.com',
            'password' => '1234561',
        ];
        $client->jsonRequest('POST', $router->generate('authentication_token'), $incorrectPasswordRequest);

        $this->assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);

        $client->jsonRequest('POST', $router->generate('authentication_token'), $request);
        $response = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertNotEmpty($response['token']);
    }
}
